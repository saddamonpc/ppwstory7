from django.test import TestCase, Client
from django.urls import resolve
from django.apps import apps
from .apps import *
from .views import *
from .models import Status
from .forms import StatusForm
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import unittest
import time

# Unit Testing
class Story7Test (TestCase):
    def testApps(self):
        self.assertEqual(TugasConfig.name, 'tugas')
        self.assertEqual(apps.get_app_config('tugas').name, 'tugas')

    def testHomeURL(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def testHomeUsingTemplate(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'home.html')

    def testHomeUsingHomeFunction(self):
        found = resolve('/')
        self.assertEqual(found.func, homepage)

    def testHomeContainsGreeting(self):
        response = Client().get('/')
        response_content = response.content.decode('utf-8')
        self.assertIn("Hello!", response_content)
    
    def testModelCreateNewStatus(self): 
        newStatus = Status.objects.create(name='Saddam', status='Happy')
        countsAllNewStatus = Status.objects.all().count()
        self.assertEqual(countsAllNewStatus, 1)
    
    def testModelReturnsStatusAttributes(self):
        newStatus = Status.objects.create(name='Saddam', status='Happy')
        self.assertEqual(str(newStatus.name), newStatus.name)
        self.assertEqual(str(newStatus.status), newStatus.status)

    def testModelReturnsStatus(self):
        newStatus = Status.objects.create(name='Saddam', status='Happy')
        self.assertEqual(str(newStatus), newStatus.status)

    def testStatusFormSoNotBlank(self):
        newStatus = StatusForm({'name':'', 'status':''})
        self.assertFalse(newStatus.is_valid())

    def testConfirmationURL(self):
        response = Client().get('/confirmation')
        self.assertEqual(response.status_code, 200)

    def testConfirmationUsingConfirmationFunction(self):
        found = resolve('/confirmation')
        self.assertEqual(found.func, confirmationpage)

    def testConfirmationContainsGreeting(self):
        response = Client().get('/confirmation')
        response_content = response.content.decode('utf-8')
        self.assertIn("Are you sure?", response_content)

    def testConfirmationPageSuccessfullyPosts(self):
        response = Client().post('/confirmation', {'name': 'name', 'Test Status': 'status'}, follow=True)
        html_response = response.content.decode('utf8')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'home.html')
        self.assertIn('name', html_response) and self.assertIn('status', html_response)


# Functional Testing
class Story7FunctionalTest(unittest.TestCase):
    def setUp(self):
        chromeOptions = Options()
        chromeOptions.add_argument('--no-sandbox')
        chromeOptions.add_argument("--headless")
        self.browser = webdriver.Chrome(options=chromeOptions)

    def tearDown(self):
        self.browser.quit()

    def testForHomeTitle(self):
        self.browser.get('http://127.0.0.1:8000/')
        self.assertIn("Story 7", self.browser.title)

    def testForHomeGreeting(self):
        self.browser.get('http://127.0.0.1:8000/')
        self.assertIn("Hello!", self.browser.page_source)

    def testForConfirmationTitle(self):
        self.browser.get('http://127.0.0.1:8000/confirmation')
        self.assertIn("Confirmation", self.browser.title)

    def testForConfirmationGreeting(self):
        self.browser.get('http://127.0.0.1:8000/confirmation')
        self.assertIn("Are you sure?", self.browser.page_source)

    def testForChangingColour(self):
        self.browser.get('http://127.0.0.1:8000/')
        name = self.browser.find_element_by_name('name')
        name.send_keys("Test Name")
        time.sleep(5)
        status = self.browser.find_element_by_name('status')
        status.send_keys("Test Status")
        time.sleep(5)
        button = self.browser.find_element_by_id('postStatus')
        button.submit()
        time.sleep(5)
        button = self.browser.find_element_by_name('yes')
        button.submit()

    def testForFormSubmission(self):
        self.browser.get('http://127.0.0.1:8000/')
        name = self.browser.find_element_by_name('name')
        name.send_keys("Test Name")
        time.sleep(5)
        status = self.browser.find_element_by_name('status')
        status.send_keys("Test Status")
        time.sleep(5)
        button = self.browser.find_element_by_id('postStatus')
        button.submit()
        self.assertIn("Are you sure?", self.browser.page_source)

    def testForFormWithoutConfirmationSubmission(self):
        self.browser.get('http://127.0.0.1:8000/')
        name = self.browser.find_element_by_name('name')
        name.send_keys("Test Name")
        time.sleep(5)
        status = self.browser.find_element_by_name('status')
        status.send_keys("Test Status")
        time.sleep(5)
        button = self.browser.find_element_by_id('postStatus')
        button.submit()
        time.sleep(5)
        button = self.browser.find_element_by_name('no')
        button.submit()
        self.assertIn("Story 7", self.browser.title) and self.assertNotIn("Test Name", self.browser.page_source) and self.assertNotIn("Test Status", self.browser.page_source)

    def testForFormWithConfirmationSubmission(self):
        self.browser.get('http://127.0.0.1:8000/')
        name = self.browser.find_element_by_name('name')
        name.send_keys("Test Name")
        time.sleep(5)
        status = self.browser.find_element_by_name('status')
        status.send_keys("Test Status")
        time.sleep(5)
        button = self.browser.find_element_by_id('postStatus')
        button.submit()
        time.sleep(5)
        button = self.browser.find_element_by_name('yes')
        button.submit()
        self.assertIn("Story 7", self.browser.title) and self.assertIn("Test Name", self.browser.page_source) and self.assertIn("Test Status", self.browser.page_source)

    def testForBeforeChangingColour(self):
        self.browser.get('http://127.0.0.1:8000/')
        element = self.browser.find_element_by_name("statusForChallenge")
        colour = element.value_of_css_property("color")
        self.assertEqual(colour, "rgba(53, 82, 111, 1)");

    def testForAfterChangingColour(self):
        self.browser.get('http://127.0.0.1:8000/')
        button = self.browser.find_element_by_name('changeColour')
        button.click()
        element = self.browser.find_element_by_name("statusForChallenge")
        colour = element.value_of_css_property("color")
        self.assertEqual(colour, "rgba(255, 0, 0, 1)");
