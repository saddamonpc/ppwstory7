from django.db import models

# Create your models here.
class Status(models.Model):
    name = models.CharField(max_length=50, unique=True)
    status = models.CharField(max_length=100, primary_key=True)

    def __str__(self):
        return self.status

