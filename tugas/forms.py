from django import forms
from .models import Status

class StatusForm(forms.Form):
    name = forms.CharField(widget=forms.TextInput(attrs={
        'class' : 'form-control',
        'type' : 'text',
        'maxlength' : '50',
        'required' : True,
    }))
    status = forms.CharField(widget=forms.TextInput(attrs={
        'class' : 'form-control',
        'type' : 'text',
        'maxlength' : '100',
        'required' : True,
    }))
