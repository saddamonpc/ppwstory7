from django.shortcuts import render, redirect
from .forms import StatusForm
from .models import Status

# Create your views here.
def homepage(request):
    if request.method == "POST":
        form = StatusForm(request.POST)
        if (form.is_valid()):
            newStatus = Status()
            newStatus.name = form.cleaned_data['name']
            newStatus.status = form.cleaned_data['status']
            response = {
                'name' : newStatus.name,
                'status' : newStatus.status,
            }
            return render(request,'confirmation.html', response)
    else:
        form = StatusForm()
    status = Status.objects.all()
    response = {'status': status, 'form':form}
    return render(request,'home.html', response)

def confirmationpage(request):
    if request.method == "POST":
        form = StatusForm(request.POST)
        if (form.is_valid()):
            newStatus = Status()
            newStatus.name = form.cleaned_data['name']
            newStatus.status = form.cleaned_data['status']
            newStatus.save()
        return redirect('/')
    else:
        form = StatusForm()
    response = {'form':form}
    return render(request,'confirmation.html', response)
    